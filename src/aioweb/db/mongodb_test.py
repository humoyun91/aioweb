import unittest
import asyncio
from aioweb.db import MongoDBAdapter, get_db
from aioweb.db.model import Model
from pymongo.mongo_client import MongoClient
from aioweb.test import TestCase, TestModel, run_briefly


class MongoDBTestCase(TestCase):
    def setUp(self):
        super(MongoDBTestCase, self).setUp()
        self.db = get_db()
        resp = self.loop.run_until_complete(self.db.info())
        assert 'version' in resp

    def tearDown(self):
        r = self.loop.run_until_complete(self.db.drop_db())
        super(MongoDBTestCase, self).tearDown()


class MongoDBAdapterTest(MongoDBTestCase):
    def test_server_info(self):
        resp = self.loop.run_until_complete(self.db.info())
        assert 'version' in resp

    def test_put(self):
        data = TestModel(name='test')
        resp = self.loop.run_until_complete(self.db.put(data))
        assert hasattr(resp, '_id'), resp.__dict__

    def test_mongo(self):
        client = MongoClient()
        db = client.test_database
        collection = db.test_collection
        import datetime
        post = {"author": "Mike",
                "text": "My first blog post!",
                "tags": ["mongodb", "python", "pymongo"],
                "date": datetime.datetime.utcnow()}
        posts = db.posts
        post_id = posts.insert(post)
        print(post_id)
        print(client.server_info())

