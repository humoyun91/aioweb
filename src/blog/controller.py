from aioweb.controller import Controller
from datetime import datetime


class HomeController(Controller):
    def __init__(self, db=None, post_source='post', comment_source='comment', 
                 **kwargs):
        super(HomeController, self).__init__(db, **kwargs)
        self.blog_source = post_source
        self.comment_source = comment_source

    def all_posts(self):
        yield from Post.all(self.db)

    def store_query(self, query):
        query['datetime'] = datetime.now()
        return query

    def new_post(self, model):
        assert 'title' in model
        assert 'datetime' in model
        assert 'body' in model
        model['doc_type'] = 'Post'
        return self.db.put(model)
