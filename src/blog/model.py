import colander

#class Post(Model):
#    required_fields = ['title', 'body', 'date']


class NoneAcceptantNode(colander.SchemaNode):
    """Accepts None values for schema nodes.
    """
    def deserialize(self, value):
        if value is not None:
            return super(NoneAcceptantNode, self).deserialize(value)

    def serialize(self, appstruct):
        if not appstruct:
            return ""
        return super(NoneAcceptantNode, self).serialize(appstruct)


class Model(colander.MappingSchema):
    _id = NoneAcceptantNode(colander.String(), missing="")

    @classmethod
    def to_json(cls, obj):
        inst = cls()
        return inst.serialize(obj)

    @classmethod
    def _get_doc_type(cls):
        return cls.__name__

    @classmethod
    def all(cls):
        return getattr(cls, cls.db).find()


class Post(Model):
    title = colander.SchemaNode(colander.String())
    datetime = colander.SchemaNode(colander.DateTime())
    body = colander.SchemaNode(colander.String())
