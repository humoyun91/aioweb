#!/usr/bin/python3
"""
Script to setup env and run a development server for app
Does the following:
- setup sys path to libraries included
- change dir to src
- execvpe sys.argv[1] with parameters passed and default environ


Usage:
    aioweb-launch.py startapp <project>
    aioweb-launch.py execute <command>

Options:
    -h --help Show this 
"""
from docopt import docopt
import os
from os.path import join, dirname, abspath
import sys
import site

paths = []

base_path = abspath(os.getcwd())
print(base_path)
src_path = join(base_path, "src")
lib_path = join(base_path, "lib")
paths.append(src_path)
sys.path.extend(paths)
site.addsitedir(lib_path)
os.chdir(src_path)
environ = {}
environ['PYTHONPATH'] = ":".join(sys.path)
environ.update(os.environ)
arguments = docopt(__doc__, version='0.1')
print(arguments)
if arguments['execute']:
    os.execvpe(sys.argv[2], sys.argv[2:], environ)
elif arguments['startapp']:
    os.execvpe("python3", sys.argv[1:], environ)
