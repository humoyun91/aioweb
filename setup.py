"""Asynchronous web framework, based on asyncio

Webserver using the asyncio(pep3156) modules.

"""

classifiers = """\
Development Status :: 2 - Pre-Alpha
Intended Audience :: Developers
License :: OSI Approved :: Apache Software License
Programming Language :: Python
Topic :: Internet :: WWW/HTTP :: HTTP Servers
Topic :: Software Development :: Libraries :: Python Modules
Operating System :: Unix
"""
from setuptools import setup, find_packages

doclines = __doc__.split("\n")


setup(
    name="aioweb",
    version="0.1",
    url='https://bitbucket.org/jagguli/aioweb',
    maintainer_email="steven@stevenjoseph.in",
    license='BSD',
    description="A minimal framework for webapps using asyncio",
    author='Steven Joseph',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=['setuptools', 'asyncio', 'aiohttp'],
    data_files=[('aioweb/conf', [
        'src/aioweb/conf/logging.json',
        'src/aioweb/conf/default.ini',
    ])],
    scripts=['bin/aioweb-launch.py'],
    license="http://www.apache.org/licenses/LICENSE-2.0.txt",
    platforms=["any"],
    description=doclines[0],
    classifiers=filter(None, classifiers.split("\n")),
    long_description="\n".join(doclines[2:]),
)
